var express = require('express');
var cors = require('cors');


const app = express();
app.use(cors());

app.get('/', function (req, res) {
  console.log(req.query);
  var result = 'Invalid username';

  if(req.query.username){
//    const re = new RegExp('^(?:https?\\:)?\\@*(?:(?://)?\\w+\\.\\w+(?:\\.\\w+)*/)?(.*?)(?:(?:\\?|/).*)?$','');
    const re = new RegExp('^(?:https?\\:)?\\@*(?:(?://)?.*?/)?\\@*(.*?)(?:(?:\\?|/).*)?$','');
    const username = re.exec(req.query.username);
    if( username[1] )
      result = '@'+username[1];
    console.log(username);

  }
  res.send(result);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

